from app.api import api
from app.api.api_classes import *

""" Clients """

api.add_resource(Clients, '/clients')
api.add_resource(ClientSingle, '/clients/<client_id>')

""" Cargos """

api.add_resource(Cargos, '/cargos')
api.add_resource(CargoSingle, '/cargos/<cargo_id>')

""" Items """

api.add_resource(Items, '/items')
api.add_resource(ItemSingle, '/items/<item_id>')

""" Drivers """

api.add_resource(Drivers, '/drivers')
api.add_resource(DriverSingle, '/drivers/<driver_id>')

""" Cars """

api.add_resource(Cars, '/cars')
api.add_resource(CarSingle, '/cars/<car_id>')


