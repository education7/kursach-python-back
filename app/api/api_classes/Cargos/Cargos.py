from app.api.api_classes import Resource, reqparse
from app.api.api_classes import Cargo
from app.api.api_classes import db


# Список всех грузов
class Cargos(Resource):
    # Настройка запроса request и его полей
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('nomenclature', type=str, required=True,
                                 help='nomenclature not provided', location='json')  # Описание
        self.parser.add_argument('weight', type=float, required=True,
                                 help='weight not provided', location='json')
        self.parser.add_argument('due_date', type=str, required=True,
                                 help='due_date not provided', location='json')
        self.parser.add_argument('address', type=str, required=True,
                                 help='address not provided', location='json')
        self.parser.add_argument('driver_id', type=str, required=True,
                                 help='address not provided', location='json')
        self.parser.add_argument('client_id', type=str, required=True,
                                 help='address not provided', location='json')
        self.parser.add_argument('comment', type=str, required=False, location='json')
        super(Cargos, self).__init__()

    # Выдать список всех объектов Cargo
    # noinspection PyMethodMayBeStatic
    def get(self):
        cargos_list = Cargo.query.all()
        data = Cargo.to_dict_list(cargos_list)
        return {'data': data}, 200

    # Создать новый объект Cargo
    # noinspection PyMethodMayBeStatic
    def post(self):
        data = self.parser.parse_args()

        cargo = Cargo()
        cargo.from_dict(data)
        db.session.add(cargo)
        db.session.commit()
        data = cargo.to_dict(False)
        return {'data': data, 'message': "Груз успешно создан"}, 201
