from app.api.api_classes import Resource, reqparse
from app.api.api_classes import Driver
from app.api.api_classes import db


# Список всех грузов
class Drivers(Resource):
    # Настройка запроса request и его полей
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('first_name', type=str, required=True,
                                 help='name not provided', location='json')
        self.parser.add_argument('middle_name', type=str, required=True,
                                 help='name not provided', location='json')
        self.parser.add_argument('last_name', type=str, required=True,
                                 help='name not provided', location='json')
        self.parser.add_argument('categories', type=str, required=True,
                                 help='name not provided', location='json')
        self.parser.add_argument('phone', type=str, required=True,
                                 help='name not provided', location='json')


    # Выдать список всех объектов Driver
    # noinspection PyMethodMayBeStatic
    def get(self):
        drivers = Driver.query.all()
        data = Driver.to_dict_list(drivers)
        return {'data': data}, 200

    # Создать новый объект Driver
    # noinspection PyMethodMayBeStatic
    def post(self):
        data = self.parser.parse_args()

        driver = Driver()
        driver.from_dict(data)
        db.session.add(driver)
        db.session.commit()
        data = driver.to_dict()
        return {'data': data, 'message': "Водитель успешно создан"}, 201
