from app.api.api_classes import Resource, reqparse
from app.api.api_classes import Driver
from app.api.api_classes import db
from app.api.extensions import compare


# Один груз
class DriverSingle(Resource):
    # Настройка запроса request и его полей
    def __init__(self):
        self.parser = reqparse.RequestParser()
        super(DriverSingle, self).__init__()

    # noinspection PyMethodMayBeStatic
    def get(self, driver_id):
        driver = Driver.query.get_or_404(driver_id)
        data = driver.to_dict()
        return {'data': data}, 200

    # noinspection PyMethodMayBeStatic
    def put(self, driver_id):
        driver = Driver.query.get_or_404(driver_id)
        data = self.parser.parse_args()

        # Здесь проверку на что-либо делать незачем

        result = compare(driver.to_dict()['attributes'], data)

        driver.from_dict(data)
        db.session.commit()
        return {'data': driver.to_dict(), 'message': "№{} успешно изменён".format(driver_id)}, 200

    # noinspection PyMethodMayBeStatic
    def delete(self, driver_id):
        driver = Driver.query.get_or_404(driver_id)
        db.session.delete(driver)
        db.session.commit()
        data = driver.to_dict()
        return {'data': data, 'message': "№{} успешно удалён".format(driver_id)}, 200
