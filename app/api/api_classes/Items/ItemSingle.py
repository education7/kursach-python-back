from app.api.api_classes import Resource, reqparse
from app.api.api_classes import Item
from app.api.api_classes import db
from app.api.extensions import compare


# Один груз
class ItemSingle(Resource):
    # Настройка запроса request и его полей
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('nomenclature', type=str, required=False, location='json')
        self.parser.add_argument('weight', type=float, required=False, location='json')
        self.parser.add_argument('application_id', type=int, required=False, location='json')
        super(ItemSingle, self).__init__()

    # noinspection PyMethodMayBeStatic
    def get(self, item_id):
        item = Item.query.get_or_404(item_id)
        data = item.to_dict()
        return {'data': data}, 200

    # Внести изменения в объект Item
    # noinspection PyMethodMayBeStatic
    def put(self, item_id):
        item = Item.query.get_or_404(item_id)
        data = self.parser.parse_args()

        # Здесь проверку на что-либо делать незачем

        result = compare(item.to_dict()['attributes'], data)

        item.from_dict(data)
        db.session.commit()
        return {'data': item.to_dict(), 'message': "№{} успешно изменён".format(item_id)}, 200

    # noinspection PyMethodMayBeStatic
    def delete(self, item_id):
        item = Item.query.get_or_404(item_id)
        db.session.delete(item)
        db.session.commit()
        data = item.to_dict()
        return {'data': data, 'message': "№{} успешно удалён".format(item_id)}, 200
