from app.api.api_classes import Resource, reqparse
from app.api.api_classes import db
from app.api.api_classes import Item



# Список всех грузов
class Items(Resource):
    # Настройка запроса request и его полей
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('name', type=str, required=True,
                                 help='name not provided', location='json') 
        self.parser.add_argument('cargo_id', type=str, required=True,
                                 help='name not provided', location='json') 

    # Выдать список всех объектов Cargo
    # noinspection PyMethodMayBeStatic
    def get(self):
        items = Item.query.all()
        data = Item.to_dict_list(items)
        return {'data': data}, 200

    # Создать новый объект Cargo
    # noinspection PyMethodMayBeStatic
    def post(self):
        data = self.parser.parse_args()


        item = Item()
        item.from_dict(data)
        db.session.add(item)
        db.session.commit()
        data = item.to_dict()
        return {'data': data, 'message': "Товар успешно создан"}, 201
