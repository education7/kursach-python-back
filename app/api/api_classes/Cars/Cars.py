from app.api.api_classes import Resource, reqparse
from app.api.api_classes import db
from app.api.api_classes import Car



# Список всех грузов
class Cars(Resource):
    # Настройка запроса request и его полей
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('model', type=str, required=True,
                                 help='name not provided', location='json')
        self.parser.add_argument('weight', type=str, required=True,
                                 help='name not provided', location='json')
        self.parser.add_argument('driver_id', type=str, required=True,
                                 help='name not provided', location='json')

    # Выдать список всех объектов Car
    # noinspection PyMethodMayBeStatic
    def get(self):
        cars = Car.query.all()
        data = Car.to_dict_list(cars)
        return {'data': data}, 200

    # Создать новый объект Car
    # noinspection PyMethodMayBeStatic
    def post(self):
        data = self.parser.parse_args()

        car = Car()
        car.from_dict(data)
        db.session.add(car)
        db.session.commit()
        data = car.to_dict()
        return {'data': data, 'message': "Машина создана"}, 201
