from app.api.api_classes import Resource, reqparse
from app.api.api_classes import Car
from app.api.api_classes import db
from app.api.extensions import compare


# Один груз
class CarSingle(Resource):
    # Настройка запроса request и его полей
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('weight', type=float, required=False, location='json')
        self.parser.add_argument('category', type=float, required=False, location='json')
        super(CarSingle, self).__init__()

    # noinspection PyMethodMayBeStatic
    def get(self, car_id):
        car = Car.query.get_or_404(car_id)
        data = car.to_dict()
        return {'data': data}, 200

    # Внести изменения в объект Item
    # noinspection PyMethodMayBeStatic
    def put(self, car_id):
        car = Car.query.get_or_404(car_id)
        data = self.parser.parse_args()

        # Здесь проверку на что-либо делать незачем

        result = compare(car.to_dict()['attributes'], data)

        car.from_dict(data)
        db.session.commit()
        return {'data': car.to_dict(), 'message': "№{} успешно изменён".format(car_id)}, 200

    # noinspection PyMethodMayBeStatic
    def delete(self, car_id):
        car = Car.query.get_or_404(car_id)
        db.session.delete(car)
        db.session.commit()
        return {'message': "№{} успешно удалён".format(car_id)}, 200
