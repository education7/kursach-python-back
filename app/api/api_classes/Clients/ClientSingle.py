from app.api.api_classes import Resource, reqparse
from app.api.api_classes import Client
from app.api.api_classes import db
from app.api.extensions import compare


# Один клиент
class ClientSingle(Resource):
    # Настройка запроса request и его полей
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('last_name', type=str, required=False, location='json')
        self.parser.add_argument('first_name', type=str, required=False, location='json')
        self.parser.add_argument('middle_name', type=str, required=False, location='json')
        self.parser.add_argument('email', type=str, required=False, location='json')
        self.parser.add_argument('phone', type=str, required=False, location='json')
        super(ClientSingle, self).__init__()

    # Получить объект Client
    # noinspection PyMethodMayBeStatic
    def get(self, client_id):
        client = Client.query.get_or_404(client_id)
        data = client.to_dict()
        return {'data': data}, 200

    # Внести изменения в объект Client
    # noinspection PyMethodMayBeStatic
    def put(self, client_id):
        client = Client.query.get_or_404(client_id)
        data = self.parser.parse_args()

        # Проверяем какие поля хотят изменить запросом
        result = compare(client.to_dict()['attributes'], data)

        for attribute in result.keys():
            if not result[attribute] and data[attribute] is not None:
                # Если изменяют телефон
                if attribute == 'phone':

                    # Проверка на правильность телефонного номера
                    if len(data[attribute]) > 11 or data[attribute][0] not in ['7', '8']:
                        return {'message': "incorrect phone format"}, 409

                    # Если клиент с таким телефоном уже есть
                    if Client.query.filter_by(phone=data[attribute]).first():
                        return {'message': "Client with this phone already exists"}, 409

                # Если изменяют e-mail
                elif attribute == 'email':

                    # Если клиент с таким e-mail уже есть
                    if Client.query.filter_by(email=data['email']).first():
                        return {'message': "Client with this e-mail address already exists"}, 409

                # Если изменяют что-то из ФИО
                elif attribute in ['first_name', 'last_name', 'middle_name']:
                    if Client.query.filter_by(first_name=data[attribute],
                                              last_name=data[attribute],
                                              middle_name=data[attribute]).first():
                        return {'message': "Client with this full name already exists"}, 409

        client.from_dict(data)
        db.session.commit()
        return {'data': client.to_dict(), 'message': "Клиент №{} успешно изменён".format(client_id)}, 200

    # Удалить объект Client
    # noinspection PyMethodMayBeStatic
    def delete(self, client_id):
        client = Client.query.get_or_404(client_id)

        # Если хотят удалить клиента, у которого есть активные контракты
        
        db.session.delete(client)
        db.session.commit()
        return {'message': "Клиент №{} успешно удалён".format(client_id)}, 200
