from app.api.api_classes import Resource, reqparse
from app.api.api_classes import Client
from app.api.api_classes import db


# Список всех клиентов
class Clients(Resource):
    # Настройка запроса request и его полей
    def __init__(self):
        self.parser = reqparse.RequestParser()
        self.parser.add_argument('last_name', type=str, required=True,
                                 help='last name not provided', location='json')
        self.parser.add_argument('first_name', type=str, required=True,
                                 help='first name not provided', location='json')
        self.parser.add_argument('middle_name', type=str, required=False, location='json')
        self.parser.add_argument('email', type=str, required=False, location='json')
        self.parser.add_argument('phone', type=str, required=True,
                                 help='phone not provided', location='json')
        super(Clients, self).__init__()

    # Выдать список всех объектов Client
    # noinspection PyMethodMayBeStatic
    def get(self):
        clients = Client.query.all()
        data = Client.to_dict_list(clients)
        return {'data': data}, 200

    # Создать новый объект Client
    # noinspection PyMethodMayBeStatic
    def post(self):
        data = self.parser.parse_args()

    

        client = Client()
        client.from_dict(data)
        db.session.add(client)
        db.session.commit()
        data = client.to_dict()
        return {'data': data, 'message': "Клиент успешно создан"}, 201
