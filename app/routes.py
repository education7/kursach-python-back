from flask import render_template

from app import app


@app.route('/')
def test():
    data = {
            "Test" : 'This is test api fuction',
            "brand": "Ford",
            "model": "Mustang",
            "year": 1964
            }
    return {'data': data}, 200
