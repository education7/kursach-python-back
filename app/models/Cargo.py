from app.models import db

from app.models.Item import Item

class Cargo(db.Model):
    __tablename__ = 'cargos'

    """ Свои поля """
    cargo_id = db.Column(db.Integer, primary_key=True)
    nomenclature = db.Column(db.String(64), nullable=False)
    weight = db.Column(db.Float, nullable=False)
    address = db.Column(db.Text, nullable=True)
    due_date = db.Column(db.Text, nullable=True)
    comment = db.Column(db.Text, nullable=True)

    driver_id = db.Column(db.Integer, db.ForeignKey('drivers.driver_id'))
    driver = db.relationship('Driver', backref='application', uselist=False)

    client_id = db.Column(db.Integer, db.ForeignKey('clients.client_id'))
    client = db.relationship('Client', backref='application', uselist=False)

    items = db.relationship('Item', backref='requisite', lazy='dynamic')

    # Словарь полей для корректного использования setattr
    fields = {
        'nomenclature': not None, 
        'address': not None, 
        'weight': not None,
        'due_date': not None,
        'driver_id': not None,
        'client_id': not None,
        'comment': not None,
        }

    # Преобразование объекта Application в словарь
    def to_dict(self, get = True):
        if get:
            data = {
                'id': self.cargo_id,
                'items':  Item.to_dict_list(self.items),
                'attributes':
                    {
                        'nomenclature': self.nomenclature,
                        'weight': self.weight,
                        'address': self.address,
                        'due_date': self.due_date,
                        'comment': self.comment,
                        'driver_id': self.driver_id,
                        'client_id': self.client_id,
                        'driver': ' '.join([self.driver.last_name, self.driver.first_name, self.driver.middle_name or '']),
                        'client': ' '.join([self.client.last_name, self.client.first_name, self.client.middle_name or '']),

                    }
            }
            return data
        data = {
            'id': self.cargo_id,
            'items':  Item.to_dict_list(self.items),
            'attributes':
                {
                    'nomenclature': self.nomenclature,
                    'weight': self.weight,
                    'address': self.address,
                    'due_date': self.due_date,
                    'comment': self.comment,
                    'driver_id': self.driver_id,
                    'client_id': self.client_id,
                    # 'driver': ' '.join([self.driver.last_name, self.driver.first_name, self.driver.middle_name or '']),
                    # 'client': ' '.join([self.client.last_name, self.client.first_name, self.client.middle_name or '']),

                }
        }
        return data

        

    # Преобразование списка объектов Cargo в словарь
    @staticmethod
    def to_dict_list(list_data):
        data = [
            {
                'id': data.cargo_id,
                'attributes':
                    {
                        'nomenclature': data.nomenclature,
                        'weight': data.weight,
                        'address': data.address,
                        'due_date': data.due_date,
                        'comment': data.comment,
                    }
            }
            for data in list_data]
        return data

    # Извлечение доступных not null данных из словаря в объект
    def from_dict(self, data):
        for field in self.fields:
            # Пихаем значение, если оно не None и относиться к нашим полям, либо если оно None и поле может быть таким
            if field in data:
                if data[field] is not None:
                    setattr(self, field, data[field])
                elif data[field] is None and self.fields[field] is None:
                    setattr(self, field, data[field])

    def __repr__(self):
        return "<Order № {} (order number: {}>".format(self.nomenclature, self.cargo_id)
