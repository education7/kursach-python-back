from app.models import db

class Item(db.Model):
    __tablename__ = 'items'

    """ Свои поля """
    item_id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.String(64), nullable=False)

    cargo_id = db.Column(db.Integer, db.ForeignKey('cargos.cargo_id'))
    cargo = db.relationship('Cargo', backref='application', lazy='subquery', uselist=False)

    # Словарь полей для корректного использования setattr
    fields = {
        'name': not None,
        'cargo_id': not None,
        }

    # Преобразование объекта в словарь
    def to_dict(self):
        data = {
            'id': self.item_id,
            'attributes':
                {
                    'name': self.name,
                    'cargo_id': self.cargo_id,
                }
        }
        return data

    # Преобразование списка объектов Items в словарь
    @staticmethod
    def to_dict_list(list_data):
        data = [
            {
                'id': data.item_id,
                'attributes':
                    {
                        'name': data.name,
                    }
            }
            for data in list_data]
        return data

    # Извлечение доступных not null данных из словаря в объект типа Application
    def from_dict(self, data):
        for field in self.fields:
            # Пихаем значение, если оно не None и относиться к нашим полям, либо если оно None и поле может быть таким
            if field in data:
                if data[field] is not None:
                    setattr(self, field, data[field])
                elif data[field] is None and self.fields[field] is None:
                    setattr(self, field, data[field])

    def __repr__(self):
        return "<Item № {} (order number: {}>".format(self.name, self.item_id)
