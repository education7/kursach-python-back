from app import db
from app.models.Client import Client
from app.models.Cargo import Cargo
from app.models.Item import Item
from app.models.Driver import Driver
from app.models.Car import Car
