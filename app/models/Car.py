from app.models import db

from app.models.Driver import Driver


class Car(db.Model):
    __tablename__ = 'cars'

    """ Свои поля """
    car_id = db.Column(db.Integer, primary_key=True)
    weight = db.Column(db.String(64), nullable=False)
    model = db.Column(db.String(64), nullable=False)

    driver_id = db.Column(db.Integer, db.ForeignKey('drivers.driver_id'))
    driver = db.relationship('Driver', backref='car_drivers', uselist=False)

    fields = {
            'model': not None, 
            'weight': not None, 
            'driver_id': not None,
            }

    # Преобразование объекта в словарь
    def to_dict(self):
        data = {
            'id': self.car_id,
            'driver':  Driver.to_dict(self.driver),
            'attributes':
                {
                    'weight': self.weight,
                    'model': self.model,
                    'driver': ' '.join([self.driver.last_name, self.driver.first_name, self.driver.middle_name or '']),

                }
        }
        return data

    # Преобразование списка объектов Car в словарь
    @staticmethod
    def to_dict_list(list_data):
        data = [
            {
                'id': data.car_id,
                'attributes':
                    {
                        'weight': data.weight,
                        'model': data.model,
                        'driver_id': data.driver_id,
                        'driver': ' '.join([data.driver.last_name, data.driver.first_name, data.driver.middle_name or '']),

                    }
            } for data in list_data]
        return data

    # Извлечение доступных not null данных из словаря в объект типа Application
    def from_dict(self, data):
        for field in self.fields:
            # Пихаем значение, если оно не None и относиться к нашим полям, либо если оно None и поле может быть таким
            if field in data:
                if data[field] is not None:
                    setattr(self, field, data[field])
                elif data[field] is None and self.fields[field] is None:
                    setattr(self, field, data[field])

    def __repr__(self):
        return "<Car № {} (car number: {}>".format(self.cargo_id)
